import 'dart:developer';
import 'dart:io';
import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import 'machineDetail.dart';

class EdittingStore extends StatefulWidget {
  String userId;

  EdittingStore({Key key, this.userId}) : super(key: key);

  @override
  _EdittingStoreState createState() => _EdittingStoreState(this.userId);
}

class _EdittingStoreState extends State<EdittingStore> {
  String userId;
  String name = '';
  String info = '';
  double latitude = 0.0;
  double longitude = 0.0;
  LatLng location = LatLng(0, 0);
  String oldName = 'Test';
  List<Marker> markers = [];

  CollectionReference _location =
      FirebaseFirestore.instance.collection('locations');
  _EdittingStoreState(this.userId);
  TextEditingController _nameController = new TextEditingController();
  TextEditingController _infoController = new TextEditingController();
  final CameraPosition _initialPosition = CameraPosition(
      target: LatLng(13.279453610183248, 100.92627804980768), zoom: 15);
  GoogleMapController _controller;
  CollectionReference _machines =
      FirebaseFirestore.instance.collection('machines');
  @override
  void initState() {
    super.initState();
    if (this.userId.isNotEmpty) {
      _location.doc(this.userId).get().then((snapshot) {
        if (snapshot.exists) {
          var data = snapshot.data() as Map<String, dynamic>;
          setState(() {
            name = data['name'];
            info = data['info'];
            oldName = data['name'];
            latitude = 13.279453610183248;
            longitude = 100.92627804980768;
            _nameController.text = name;
            _infoController.text = info;
            // _ageController.text = age;
          });
        }
      });
    }
  }

  deleteStore() {
    _location.doc(userId).delete();
    StreamBuilder<QuerySnapshot>(
        stream: FirebaseFirestore.instance
            .collection("machines")
            .where('location_name', isEqualTo: oldName)
            .snapshots(),
        builder: (context, snapshot) {
          snapshot.data.docs.forEach((element) {
            print(element.toString());
            FirebaseFirestore.instance
                .collection("machines")
                .doc(element.id)
                .update({'location_name': name});
          });
          return Center(
            child: CircularProgressIndicator(),
          );
        });
  }

  updateMarkers(lat, lng) {
    // int id = Random().nextInt(100);
    markers.clear();
    setState(() {
      latitude = lat;
      longitude = lng;
      markers.add(Marker(
        position: LatLng(lat, lng),
        markerId: MarkerId('test'),
      ));
      // location = LatLng(lat, lng);
    });
  }

  updateData() {
    _location
        .doc(userId)
        .update({
          'name': name,
          'info': info,
          'location': {'Lat': latitude, 'Lng': longitude}
        })
        .then((value) => print('Updated Store'))
        .catchError((error) => print('Failed to Update Store: $error'));
  }

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('แก้ไขร้าน : ' + name),
        ),
        body: ListView(
          padding: EdgeInsets.all(20),
          children: [
            Form(
                autovalidateMode: AutovalidateMode.onUserInteraction,
                key: _formKey,
                child: Column(
                  children: [
                    TextFormField(
                      controller: _nameController,
                      decoration: InputDecoration(labelText: 'ชื่อ'),
                      onChanged: (value) {
                        setState(() {
                          name = value;
                        });
                      },
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'กรุณาใส่ชื่อ';
                        }
                        return null;
                      },
                    ),
                    TextFormField(
                      controller: _infoController,
                      decoration: InputDecoration(labelText: 'รายละเอียด'),
                      onChanged: (value) {
                        setState(() {
                          info = value;
                        });
                      },
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'กรุณาใส่รายละเอียด';
                        }
                        return null;
                      },
                    ),
                  ],
                )),
            Divider(),
            Container(
              height: 300.0,
              child: GoogleMap(
                initialCameraPosition: _initialPosition,
                mapType: MapType.normal,
                onMapCreated: (controller) {
                  setState(() {
                    _controller = controller;
                    // updateMarker();
                  });
                },
                markers: markers.toSet(),
                onTap: (cordinate) {
                  _controller.animateCamera(CameraUpdate.newLatLng(cordinate));
                  location = LatLng(cordinate.latitude, cordinate.longitude);
                  updateMarkers(cordinate.latitude, cordinate.longitude);
                  // print(cordinate.latitude);
                },
              ),
            ),
            ElevatedButton(
                onPressed: () {
                  if (location.latitude == 0 || location.longitude == 0) {
                    showDialog<String>(
                      context: context,
                      builder: (BuildContext context) => AlertDialog(
                        title: const Text('กรุณาใส่ตำแหน่ง'),
                        content: const Text('กรุณาปักหมุดบนแผนที่'),
                        actions: <Widget>[
                          TextButton(
                            onPressed: () => Navigator.pop(context, 'Cancel'),
                            child: const Text('ตกลง'),
                          ),
                        ],
                      ),
                    );
                  } else if (name.isEmpty) {
                    showDialog<String>(
                      context: context,
                      builder: (BuildContext context) => AlertDialog(
                        title: const Text('กรุณาใส่ชื่อ'),
                        actions: <Widget>[
                          TextButton(
                            onPressed: () => Navigator.pop(context, 'Cancel'),
                            child: const Text('ตกลง'),
                          ),
                        ],
                      ),
                    );
                  } else if (info.isEmpty) {
                    showDialog<String>(
                      context: context,
                      builder: (BuildContext context) => AlertDialog(
                        title: const Text('กรุณาใส่รายละเอียด'),
                        actions: <Widget>[
                          TextButton(
                            onPressed: () => Navigator.pop(context, 'Cancel'),
                            child: const Text('ตกลง'),
                          ),
                        ],
                      ),
                    );
                  } else if (location.latitude != 0 &&
                      location.longitude != 0 &&
                      name.isNotEmpty &&
                      info.isNotEmpty) {
                    showDialog<String>(
                      context: context,
                      builder: (BuildContext context) => AlertDialog(
                        title: const Text('คุณต้องการแก้ไขใช่หรือไม่'),
                        // content: const Text('กรุณาปักหมุดบนแผนที่'),
                        actions: <Widget>[
                          TextButton(
                            onPressed: () {
                              updateData();
                              Navigator.pop(context, 'OK');
                              Navigator.pop(context, 'OK');
                            },
                            child: const Text('ยืนยัน'),
                          ),
                          TextButton(
                            onPressed: () {
                              Navigator.pop(context, 'Cancel');
                            },
                            child: const Text('ยกเลิก'),
                          ),
                        ],
                      ),
                    );
                    // Navigator.pop(context);
                  }
                },
                child: Text('Save'))
          ],
        ),
        bottomNavigationBar: BottomAppBar(
          shape: CircularNotchedRectangle(),
          notchMargin: 8.0,
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              IconButton(
                icon: Icon(
                  Icons.arrow_back_outlined,
                ),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
              SizedBox(width: 120.0),
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton(
          backgroundColor: Colors.red[600],
          onPressed: () {
            showDialog<String>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: const Text('คุณต้องการลบร้านใช่หรือไม่'),
                // content: const Text('กรุณาปักหมุดบนแผนที่'),
                actions: <Widget>[
                  TextButton(
                    onPressed: () {
                      deleteStore();
                      Navigator.pop(context, 'OK');
                      Navigator.pop(context, 'OK');
                      // Navigator.push(
                      //     context,
                      //     MaterialPageRoute(
                      //         builder: (context) => MapWidget()));
                    },
                    child: const Text('ยืนยัน'),
                  ),
                  TextButton(
                    onPressed: () {
                      Navigator.pop(context, 'Cancel');
                    },
                    child: const Text('ยกเลิก'),
                  ),
                ],
              ),
            );
          },
          child: Icon(Icons.delete),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.endDocked);
  }
}
