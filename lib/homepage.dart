import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:map_test/adminHomepage.dart';
import 'package:map_test/map.dart';

import 'stores.dart';
import 'symbols.dart';

class HomepageWidget extends StatefulWidget {
  HomepageWidget({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _HomepageWidgetStage createState() => _HomepageWidgetStage();
}

class _HomepageWidgetStage extends State<HomepageWidget> {
  @override
  Widget build(BuildContext context) {
    return ListView(padding: EdgeInsets.all(20), children: <Widget>[
      Container(
        margin: const EdgeInsets.only(top: 40.0),
      ),
      GestureDetector(
        child: Card(
          child: new InkWell(
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => MapWidget()));
            },
            child: Container(
              width: 100.0,
              height: 200.0,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15.0),
                  image: DecorationImage(
                      fit: BoxFit.cover,
                      image: NetworkImage(
                          'https://www.xda-developers.com/files/2019/06/google-maps-india.jpg'))),
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Text(
                  'แผนที่',
                  style: GoogleFonts.prompt(
                    fontSize: 20,
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
      GestureDetector(
        child: Card(
          child: new InkWell(
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => StoreInformation()));
            },
            child: Container(
              height: 110,
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(20)),
              child: Row(
                children: [
                  Container(
                    width: 100.0,
                    height: 100.0,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15.0),
                        image: DecorationImage(
                            fit: BoxFit.cover,
                            image: NetworkImage(
                                'https://firebasestorage.googleapis.com/v0/b/convenience-store-app.appspot.com/o/location2.png?alt=media&token=b4d92ee9-d846-4725-a36c-482a71d3840a'))),
                  ),
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.only(top: 35, left: 10, right: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'รายชื่อร้าน',
                                style: GoogleFonts.prompt(
                                    fontSize: 20, height: 1.5),
                              ),
                              Icon(
                                Icons.arrow_forward_ios_outlined,
                                size: 15,
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
      GestureDetector(
        child: Card(
          child: new InkWell(
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => Symbols()));
            },
            child: Container(
              height: 110,
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(20)),
              child: Row(
                children: [
                  Container(
                    width: 100.0,
                    height: 100.0,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15.0),
                        image: DecorationImage(
                            fit: BoxFit.cover,
                            image: NetworkImage(
                                'https://firebasestorage.googleapis.com/v0/b/convenience-store-app.appspot.com/o/symbol.png?alt=media&token=517d4606-28b0-453a-a675-4e0fb406577f'))),
                  ),
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.only(top: 35, left: 10, right: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'สัญลักษณ์',
                                style: GoogleFonts.prompt(
                                    fontSize: 20, height: 1.5),
                              ),
                              Icon(
                                Icons.arrow_forward_ios_outlined,
                                size: 15,
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    ]);
  }
}
