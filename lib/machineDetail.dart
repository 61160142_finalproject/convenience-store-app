import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'machineDetail.dart';

class MachineDetail extends StatefulWidget {
  String id;
  String location;
  MachineDetail({Key key, this.id, this.location}) : super(key: key);

  @override
  _MachineDetailState createState() =>
      _MachineDetailState(this.id, this.location);
}

class _MachineDetailState extends State<MachineDetail> {
  @override
  void initState() {
    super.initState();
    setState(() {
      this.id = id;
      this.location = location;
    });
  }

  String id;
  String location;
  bool checkBT = false;
  _MachineDetailState(this.id, this.location);
  // final Stream<QuerySnapshot> _machines = FirebaseFirestore.instance
  //     .collection('machines')
  //     .where("name", isEqualTo: id)
  //     .snapshots();

  CollectionReference machines =
      FirebaseFirestore.instance.collection('machines');
  checkStatus(name, status, time, location) {
    var lateUse =
        DateTime.fromMicrosecondsSinceEpoch(time.microsecondsSinceEpoch);
    var later = lateUse.add(const Duration(minutes: 10));
    var now = DateTime.now();
    // print("lateUse :" + lateUse.toString());
    // print("Later :" + later.toString());
    // print("now :" + now.toString());
    // print(lateUse.day);
    var left = later.minute - now.minute;
    if (status == true) {
      if (left <= 0) {
        return checkButton();
      } else if (now.minute < lateUse.minute) {
        return checkButton();
      } else {
        if (lateUse.hour == now.hour &&
            lateUse.day == now.day &&
            lateUse.month == now.month) {
          checkBT = false;

          return Text(
            'เหลือเวลา ~ ' + left.toString() + " นาที ",
            style: GoogleFonts.prompt(
                fontSize: 20,
                fontWeight: FontWeight.w500,
                //
                color: Colors.red[800]),
          );
        } else {
          return checkButton();
        }
      }
    } else {
      checkBT = false;
      return Text(
        'เครื่องไม่พร้อมใช้งาน',
        style: GoogleFonts.prompt(
            fontSize: 20,
            fontWeight: FontWeight.w500,
            //
            color: Colors.red[800]),
      );
    }
  }

  checkButton() {
    checkBT = true;
    return Row(
      children: [
        Text(
          'ว่าง',
          style: GoogleFonts.prompt(
              fontSize: 20,
              fontWeight: FontWeight.w500,
              //
              color: Colors.green[800]),
        ),
      ],
    );
  }

  showButton(id, name, status, time, location) {
    if (status == false || checkBT == false) {
      return Divider();
    } else {
      return Center(
          child: Form(
              child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          primary: Colors.blue[800], // background
        ),
        onPressed: () {
          machines
              .doc(id) // <-- Doc ID where data should be updated.
              .update({'lateUse': Timestamp.now()})
              .then((value) => print('Updated'))
              .catchError((error) => print('Failed to add user: $error'));

          //   machines
          //       .doc('name')
          //       .update({'name': name, 'location_name': location});
        },
        child: Text(
          'ใช้งาน',
          style: GoogleFonts.prompt(),
        ),
      )));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          // id.toString() + " : ร้าน " + location.toString()
          '',
          style: GoogleFonts.prompt(),
        ),
      ),
      body: StreamBuilder<QuerySnapshot>(
        stream: FirebaseFirestore.instance
            .collection('machines')
            .where(
              "location_name",
              isEqualTo: location,
            )
            .where('name', isEqualTo: id)
            .snapshots(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else
            return ListView(
              padding: EdgeInsets.all(20),
              children: snapshot.data.docs.map((doc) {
                return Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Center(
                      child: Container(
                        width: 200.0,
                        height: 200.0,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(15.0),
                            image: DecorationImage(
                                fit: BoxFit.cover,
                                image: NetworkImage(
                                    'https://firebasestorage.googleapis.com/v0/b/convenience-store-app.appspot.com/o/machine.png?alt=media&token=8a6d2c4e-47e9-4279-9ecd-8a19035ec80e'))),
                      ),
                    ),
                    Text(
                      'รายละเอียด',
                      style: GoogleFonts.prompt(
                        fontSize: 15,
                        // fontWeight: FontWeight.w500,
                      ),
                    ),
                    Divider(
                      height: 20,
                    ),
                    Row(
                      children: [
                        Text(
                          'ชื่อร้าน : ',
                          style: GoogleFonts.prompt(
                            fontSize: 20,
                            // fontWeight: FontWeight.w500,
                          ),
                        ),
                        Text(
                          doc.data()['location_name'],
                          style: GoogleFonts.prompt(
                            fontSize: 20,
                            // fontWeight: FontWeight.w500,
                          ),
                        )
                      ],
                    ),
                    Row(
                      children: [
                        Text(
                          'เครื่อง : ',
                          style: GoogleFonts.prompt(
                            fontSize: 20,
                            // fontWeight: FontWeight.w500,
                          ),
                        ),
                        Text(
                          doc.data()['name'],
                          style: GoogleFonts.prompt(
                            fontSize: 20,
                            // fontWeight: FontWeight.w500,
                          ),
                        )
                      ],
                    ),
                    Divider(
                      height: 20,
                    ),
                    Text(
                      'สถานะ',
                      style: GoogleFonts.prompt(
                        fontSize: 15,
                        // fontWeight: FontWeight.w500,
                      ),
                    ),
                    Divider(
                      height: 20,
                    ),
                    checkStatus(doc.data()['name'], doc.data()['status'],
                        doc.data()['lateUse'], doc.data()['location_name']),
                    showButton(doc.id, doc.data()['name'], doc.data()['status'],
                        doc.data()['lateUse'], doc.data()['location_name']),
                  ],
                );
              }).toList(),
            );
        },
      ),
    );
  }
}
