import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'machineDetail.dart';

class StoreDetail extends StatefulWidget {
  String id;
  StoreDetail({Key key, this.id}) : super(key: key);

  @override
  _StoreDetailState createState() => _StoreDetailState(this.id);
}

class _StoreDetailState extends State<StoreDetail> {
  @override
  void initState() {
    super.initState();
    setState(() {
      this.id = id;
    });
  }

  String id;
  _StoreDetailState(this.id);
  // final Stream<QuerySnapshot> _machines = FirebaseFirestore.instance
  //     .collection('machines')
  //     .where("name", isEqualTo: id)
  //     .snapshots();

  CollectionReference machines =
      FirebaseFirestore.instance.collection('machines');

  checkStatus(docId, name, status, time, location) {
    var lateUse =
        DateTime.fromMicrosecondsSinceEpoch(time.microsecondsSinceEpoch);
    var later = lateUse.add(const Duration(minutes: 10));
    var now = DateTime.now();
    // print("lateUse :" + lateUse.toString());
    // print("Later :" + later.toString());
    // print("now :" + now.toString());
    // print(lateUse.day);
    var left = later.minute - now.minute;
    if (status == true) {
      if (left <= 0) {
        machines
            .doc(docId) // <-- Doc ID where data should be updated.
            .update({'available': true})
            .then((value) => print('Updated'))
            .catchError((error) => print('Failed to add user: $error'));

        return Text(
          '● ว่าง',
          style: GoogleFonts.prompt(
              fontSize: 15,
              fontWeight: FontWeight.w500,
              color: Colors.green[800]),
        );
      } else if (now.minute < lateUse.minute) {
        machines
            .doc(docId) // <-- Doc ID where data should be updated.
            .update({'available': true})
            .then((value) => print('Updated'))
            .catchError((error) => print('Failed to add user: $error'));

        return Text(
          '● ว่าง',
          style: GoogleFonts.prompt(
              fontSize: 15,
              fontWeight: FontWeight.w500,
              color: Colors.green[800]),
        );
      } else {
        if (lateUse.hour == now.hour &&
            lateUse.day == now.day &&
            lateUse.month == now.month) {
          machines
              .doc(docId) // <-- Doc ID where data should be updated.
              .update({'available': false})
              .then((value) => print('Updated'))
              .catchError((error) => print('Failed to add user: $error'));

          return Text(
            'เหลือเวลา ~ ' + left.toString() + " นาที ",
            style: GoogleFonts.prompt(
                fontSize: 15,
                fontWeight: FontWeight.w500,
                //
                color: Colors.red[800]),
          );
        } else {
          machines
              .doc(docId) // <-- Doc ID where data should be updated.
              .update({'available': true})
              .then((value) => print('Updated'))
              .catchError((error) => print('Failed to add user: $error'));

          return Text(
            '● ว่าง',
            style: GoogleFonts.prompt(
                fontSize: 15,
                fontWeight: FontWeight.w500,
                color: Colors.green[800]),
          );
        }
      }
    } else {
      return Text(
        'เครื่องไม่พร้อมใช้งาน',
        style: GoogleFonts.prompt(
            fontSize: 15,
            fontWeight: FontWeight.w500,
            //
            color: Colors.red[800]),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          id,
          style: GoogleFonts.prompt(),
        ),
      ),
      body: StreamBuilder<QuerySnapshot>(
        stream: FirebaseFirestore.instance
            .collection('machines')
            .where("location_name", isEqualTo: id)
            // .orderBy('name',)
            .snapshots(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else
            return ListView(
              children: snapshot.data.docs.map((doc) {
                return Card(
                  child: new InkWell(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => MachineDetail(
                                    location: doc.data()['location_name'],
                                    id: doc.data()['name'],
                                  )));
                    },
                    child: Container(
                      height: 110,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(20)),
                      child: Row(
                        children: [
                          Container(
                            width: 100.0,
                            height: 100.0,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15.0),
                                image: DecorationImage(
                                    fit: BoxFit.cover,
                                    image: NetworkImage(
                                        'https://firebasestorage.googleapis.com/v0/b/convenience-store-app.appspot.com/o/machine.png?alt=media&token=8a6d2c4e-47e9-4279-9ecd-8a19035ec80e'))),
                          ),
                          Expanded(
                            child: Container(
                              padding:
                                  EdgeInsets.only(top: 25, left: 10, right: 10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        doc.data()['name'],
                                        style: GoogleFonts.prompt(
                                          fontSize: 20,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                      Icon(
                                        Icons.arrow_forward_ios_outlined,
                                        size: 15,
                                      )
                                    ],
                                  ),
                                  checkStatus(
                                      doc.id,
                                      doc.data()['name'],
                                      doc.data()['status'] ??= false,
                                      doc.data()['lateUse'],
                                      doc.data()['location_name']),
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                );
              }).toList(),
            );
        },
      ),
    );
  }
}
