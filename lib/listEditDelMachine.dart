import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:map_test/editDelMachine.dart';

import 'machineDetail.dart';

class EditDelMachine extends StatefulWidget {
  EditDelMachine({Key key}) : super(key: key);

  @override
  _EditDelMachineState createState() => _EditDelMachineState();
}

class _EditDelMachineState extends State<EditDelMachine> {
  @override
  void initState() {
    super.initState();
    setState(() {});
  }

  _EditDelMachineState();
  // final Stream<QuerySnapshot> _machines = FirebaseFirestore.instance
  //     .collection('machines')
  //     .where("name", isEqualTo: id)
  //     .snapshots();

  CollectionReference machines =
      FirebaseFirestore.instance.collection('machines');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'แก้ไขเครื่องซักผ้า',
          style: GoogleFonts.prompt(),
        ),
      ),
      body: StreamBuilder<QuerySnapshot>(
        stream: FirebaseFirestore.instance
            .collection('machines')
            // .where("location_name", isEqualTo: id)
            .orderBy(
              'location_name',
            )
            .snapshots(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else
            return ListView(
              children: snapshot.data.docs.map((doc) {
                return Card(
                  child: new InkWell(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => EdittingMachine(
                                    userId: doc.id,
                                    name: doc.data()['name'],
                                    location: doc.data()['location_name'],
                                    status: doc.data()['status'],
                                  )));
                    },
                    child: Container(
                      height: 110,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(20)),
                      child: Row(
                        children: [
                          Container(
                            width: 100.0,
                            height: 100.0,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15.0),
                                image: DecorationImage(
                                    fit: BoxFit.cover,
                                    image: NetworkImage(
                                        'https://firebasestorage.googleapis.com/v0/b/convenience-store-app.appspot.com/o/machine.png?alt=media&token=8a6d2c4e-47e9-4279-9ecd-8a19035ec80e'))),
                          ),
                          Expanded(
                            child: Container(
                              padding:
                                  EdgeInsets.only(top: 25, left: 10, right: 10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        doc.data()['location_name'],
                                        style: GoogleFonts.prompt(
                                          fontSize: 20,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                      Icon(
                                        Icons.arrow_forward_ios_outlined,
                                        size: 15,
                                      )
                                    ],
                                  ),
                                  Text(
                                    doc.data()['name'],
                                    style: GoogleFonts.prompt(
                                      fontSize: 20,
                                      fontWeight: FontWeight.w500,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                );
              }).toList(),
            );
        },
      ),
    );
  }
}
