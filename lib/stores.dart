import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:map_test/adminHomepage.dart';
import 'package:map_test/storeDetail.dart';
import 'package:google_fonts/google_fonts.dart';

import 'addStore.dart';
import 'map.dart';

class StoreInformation extends StatefulWidget {
  StoreInformation({Key key}) : super(key: key);

  @override
  _StoreInformationState createState() => _StoreInformationState();
}

class _StoreInformationState extends State<StoreInformation> {
  final Stream<QuerySnapshot> _storeStream = FirebaseFirestore.instance
      .collection('locations')
      .orderBy('name', descending: false)
      .snapshots();

  machineCount(name) {
    var machines = FirebaseFirestore.instance
        .collection("machines")
        .where('location_name', isEqualTo: name)
        .where('available', isEqualTo: true)
        .where('status', isEqualTo: true)
        .snapshots();
    return StreamBuilder<QuerySnapshot>(
        stream: machines,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data.size.toString() == '0') {
              return Text(
                  'เครื่องว่าง : ' + snapshot.data.size.toString() ?? '0',
                  style: GoogleFonts.prompt(
                      fontSize: 17, height: 2, color: Colors.red[800]));
            } else {
              return Text(
                  'เครื่องว่าง : ' + snapshot.data.size.toString() ?? '0',
                  style: GoogleFonts.prompt(
                      fontSize: 17, height: 2, color: Colors.green[800]));
            }
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'รายชื่อร้าน',
          style: GoogleFonts.prompt(),
        ),
        centerTitle: true,
      ),
      body: StreamBuilder<QuerySnapshot>(
        stream: _storeStream,
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else
            return ListView(
              children: snapshot.data.docs.map((doc) {
                return Card(
                  child: new InkWell(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => StoreDetail(
                                    id: doc.data()['name'],
                                  )));
                    },
                    child: Container(
                      height: 110,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(20)),
                      child: Row(
                        children: [
                          Container(
                            width: 100.0,
                            height: 100.0,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15.0),
                                image: DecorationImage(
                                    fit: BoxFit.cover,
                                    image: NetworkImage(
                                        'https://firebasestorage.googleapis.com/v0/b/convenience-store-app.appspot.com/o/location2.png?alt=media&token=b4d92ee9-d846-4725-a36c-482a71d3840a'))),
                          ),
                          Expanded(
                            child: Container(
                              padding:
                                  EdgeInsets.only(top: 10, left: 10, right: 10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        doc.data()['name'],
                                        style: GoogleFonts.prompt(
                                          fontSize: 20,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                      Icon(
                                        Icons.arrow_forward_ios_outlined,
                                        size: 15,
                                      )
                                    ],
                                  ),
                                  Text(
                                    doc.data()['info'] ??= ' ',
                                    style: GoogleFonts.prompt(
                                        fontSize: 15, height: 1.5),
                                  ),
                                  machineCount(doc.data()['name']),
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                );
              }).toList(),
            );
        },
      ),
      bottomNavigationBar: BottomAppBar(
        shape: CircularNotchedRectangle(),
        notchMargin: 8.0,
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            SizedBox(width: 200.0),
            IconButton(
              icon: Icon(
                Icons.admin_panel_settings_outlined,
              ),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => AdminHomepageWidget()));
              },
            )
          ],
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.startDocked,
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.green[900],
        child: Icon(Icons.map_outlined),
        onPressed: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => MapWidget()));
        },
      ),
    );
  }
}
