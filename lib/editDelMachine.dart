import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import 'map.dart';

class EdittingMachine extends StatefulWidget {
  String userId;
  String name;
  String location;
  bool status;
  EdittingMachine({Key key, this.userId, this.name, this.location, this.status})
      : super(key: key);

  @override
  _EdittingMachineState createState() =>
      _EdittingMachineState(this.userId, this.name, this.location, this.status);
}

class _EdittingMachineState extends State<EdittingMachine> {
  String userId = '';
  String name = '';
  // String info = '';
  String location = '';
  bool status = false;
  CollectionReference machines =
      FirebaseFirestore.instance.collection("machines");
  TextEditingController _nameController = new TextEditingController();
  TextEditingController _infoController = new TextEditingController();
  final List<Marker> markers = [];
  // bool isSwitched = false;
  _EdittingMachineState(this.userId, this.name, this.location, this.status);
  // CollectionReference _locationCollection =
  //     FirebaseFirestore.instance.collection("locations");
  // String _chosenValue;

  @override
  void initState() {
    super.initState();
    setState(() {
      // info = "Test";
      _nameController.text = name;
      this.location = location;
      this.status = status;
    });
  }

  updateData() {
    print(status);
    machines
        .doc(userId) // <-- Doc ID where data should be updated.
        .update({'name': name, 'location_name': location, 'status': status})
        .then((value) => print('Updated'))
        .catchError((error) => print('Failed to add user: $error'));
  }

  delMachine() {
    machines.doc(userId).delete();
  }

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            "แก้ไข/ลบเครื่องซักผ้า",
            style: GoogleFonts.prompt(),
          ),
          centerTitle: true,
        ),
        body: ListView(
          padding: EdgeInsets.all(20),
          children: [
            Row(
              children: [
                Switch(
                    value: status,
                    onChanged: (value) {
                      setState(() {
                        status = value;
                      });
                      print(value);
                    }),
              ],
            ),
            Form(
                autovalidateMode: AutovalidateMode.onUserInteraction,
                key: _formKey,
                child: Column(
                  children: [
                    TextFormField(
                      controller: _nameController,
                      decoration: InputDecoration(labelText: 'ชื่อ'),
                      onChanged: (value) {
                        setState(() {
                          name = value;
                        });
                      },
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'กรุณาใส่ชื่อ';
                        }
                        return null;
                      },
                    ),
                  ],
                )),
            Divider(),
            StreamBuilder<QuerySnapshot>(
                stream: FirebaseFirestore.instance
                    .collection('locations')
                    .snapshots(),
                builder: (context, snapshot) {
                  if (!snapshot.hasData)
                    return Center(
                      child: CupertinoActivityIndicator(),
                    );

                  return Container(
                    padding: EdgeInsets.only(bottom: 16.0),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                            flex: 2,
                            child: Container(
                              padding:
                                  EdgeInsets.fromLTRB(12.0, 10.0, 10.0, 10.0),
                              child: Text(
                                'ชื่อร้าน',
                              ),
                            )),
                        new Expanded(
                          flex: 4,
                          child: DropdownButton(
                            value: location,
                            isDense: true,
                            onChanged: (valueSelectedByUser) {
                              setState(() {
                                location = valueSelectedByUser;
                              });
                              // _onShopDropItemSelected(valueSelectedByUser);
                              // info = valueSelectedByUser;
                              print(valueSelectedByUser);
                            },
                            hint: Text('เลือก'),
                            items: snapshot.data.docs
                                .map((DocumentSnapshot document) {
                              return DropdownMenuItem<String>(
                                value: document.data()['name'],
                                child: Text(document.data()['name']),
                              );
                            }).toList(),
                          ),
                        ),
                      ],
                    ),
                  );
                }),
            ElevatedButton(
                onPressed: () {
                  showDialog<String>(
                    context: context,
                    builder: (BuildContext context) => AlertDialog(
                      title: const Text('คุณต้องการแก้ไขใช่หรือไม่'),
                      // content: const Text('กรุณาปักหมุดบนแผนที่'),
                      actions: <Widget>[
                        TextButton(
                          onPressed: () {
                            updateData();
                            Navigator.pop(context, 'OK');
                            Navigator.pop(context, 'OK');
                          },
                          child: const Text('ยืนยัน'),
                        ),
                        TextButton(
                          onPressed: () {
                            Navigator.pop(context, 'Cancel');
                          },
                          child: const Text('ยกเลิก'),
                        ),
                      ],
                    ),
                  );

                },
                child: Text('Save'))
          ],
        ),
        bottomNavigationBar: BottomAppBar(
          shape: CircularNotchedRectangle(),
          notchMargin: 8.0,
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              IconButton(
                icon: Icon(
                  Icons.arrow_back_outlined,
                ),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
              SizedBox(width: 120.0),
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton(
          backgroundColor: Colors.red[600],
          onPressed: () {
            showDialog<String>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: const Text('คุณต้องการลบร้านใช่หรือไม่'),
                // content: const Text('กรุณาปักหมุดบนแผนที่'),
                actions: <Widget>[
                  TextButton(
                    onPressed: () {
                      delMachine();
                      Navigator.pop(context, 'OK');
                      Navigator.pop(context, 'OK');
                      // Navigator.push(
                      //     context,
                      //     MaterialPageRoute(
                      //         builder: (context) => MapWidget()));
                    },
                    child: const Text('ยืนยัน'),
                  ),
                  TextButton(
                    onPressed: () {
                      Navigator.pop(context, 'Cancel');
                    },
                    child: const Text('ยกเลิก'),
                  ),
                ],
              ),
            );
          },
          child: Icon(Icons.delete),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.endDocked);
  }
}
