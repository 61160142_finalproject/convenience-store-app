import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:map_test/homepage.dart';

import 'adminHomepage.dart';
import 'map.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MaterialApp(
    title: 'Convenience Store',
    home: MyApp(),
    theme: ThemeData(
      primaryColor: Colors.grey[100],
    ),
  ));
}

class MyApp extends StatelessWidget {
  const MyApp({key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(title: Text('Convenience Store')),
      bottomNavigationBar: BottomAppBar(
        shape: CircularNotchedRectangle(),
        notchMargin: 8.0,
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            SizedBox(width: 200.0),
            IconButton(
              icon: Icon(
                Icons.admin_panel_settings_outlined,
              ),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => AdminHomepageWidget()));
              },
            ),
          ],
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.startDocked,
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.green[900],
        child: Icon(Icons.map_outlined),
        onPressed: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => MapWidget()));
        },
      ),
      body: HomepageWidget(),
    );
  }
}
