import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import 'map.dart';

class AddingMachine extends StatefulWidget {
  AddingMachine({Key key}) : super(key: key);

  @override
  _AddingMachineState createState() => _AddingMachineState();
}

class _AddingMachineState extends State<AddingMachine> {
  String userId;
  String name = '';
  String info = '';
  LatLng location = LatLng(0, 0);
  CollectionReference machines =
      FirebaseFirestore.instance.collection("machines");
  TextEditingController _nameController = new TextEditingController();
  TextEditingController _infoController = new TextEditingController();
  final List<Marker> markers = [];

  // CollectionReference _locationCollection =
  //     FirebaseFirestore.instance.collection("locations");

  @override
  void initState() {
    super.initState();
    setState(() {
      info = "Test";
    });
    // users.doc(this.userId).get().then((snapshot) {
    //   if (snapshot.exists) {
    //     var data = snapshot.data() as Map<String, dynamic>;
    //     setState(() {
    //       name = data['name'];
    //       info = data['info'];
    //       // age = data['age'].toString();
    //       _nameController.text = name;
    //       _infoController.text = info;
    //       // _locationController.text = age;
    //     });
    //   }
    // }
    // );
  }

  addMachine() async {
    machines
        .add({
          'name': name,
          'location_name': _chosenValue,
          'lateUse': Timestamp.fromDate(
                  DateTime.now().subtract(const Duration(minutes: 60)))
              .toDate(),
          'status': true,
          'available': true
        })
        .then((value) => print('Add'))
        .catchError((error) => print('Failed to add user: $error'));
  }

  final _formKey = GlobalKey<FormState>();

  GoogleMapController _controller;
  final CameraPosition _initialPosition = CameraPosition(
      target: LatLng(13.279453610183248, 100.92627804980768), zoom: 15);
  String _chosenValue;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            "เพิ่มเครื่องซักผ้า",
            style: GoogleFonts.prompt(),
          ),
          centerTitle: true,
        ),
        body: ListView(
          padding: EdgeInsets.all(20),
          children: [
            Form(
                autovalidateMode: AutovalidateMode.onUserInteraction,
                key: _formKey,
                child: Column(
                  children: [
                    TextFormField(
                      controller: _nameController,
                      decoration: InputDecoration(labelText: 'ชื่อ'),
                      onChanged: (value) {
                        setState(() {
                          name = value;
                        });
                      },
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'กรุณาใส่ชื่อ';
                        }
                        return null;
                      },
                    ),
                  ],
                )),
            Divider(),
            StreamBuilder<QuerySnapshot>(
                stream: FirebaseFirestore.instance
                    .collection('locations')
                    .snapshots(),
                builder: (context, snapshot) {
                  if (!snapshot.hasData)
                    return Center(
                      child: CupertinoActivityIndicator(),
                    );

                  return Container(
                    padding: EdgeInsets.only(bottom: 16.0),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                            flex: 2,
                            child: Container(
                              padding:
                                  EdgeInsets.fromLTRB(12.0, 10.0, 10.0, 10.0),
                              child: Text(
                                'ชื่อร้าน',
                              ),
                            )),
                        new Expanded(
                          flex: 4,
                          child: DropdownButton(
                            value: _chosenValue,
                            isDense: true,
                            onChanged: (valueSelectedByUser) {
                              setState(() {
                                _chosenValue = valueSelectedByUser;
                              });
                              // _onShopDropItemSelected(valueSelectedByUser);
                              // info = valueSelectedByUser;
                              print(valueSelectedByUser);
                            },
                            hint: Text('เลือก'),
                            items: snapshot.data.docs
                                .map((DocumentSnapshot document) {
                              return DropdownMenuItem<String>(
                                value: document.data()['name'],
                                child: Text(document.data()['name']),
                              );
                            }).toList(),
                          ),
                        ),
                      ],
                    ),
                  );
                }),
            ElevatedButton(
                onPressed: () {
                  addMachine();
                  Navigator.pop(context, 'OK');

                  // if (location.latitude == 0 || location.longitude == 0) {
                  //   showDialog<String>(
                  //     context: context,
                  //     builder: (BuildContext context) => AlertDialog(
                  //       title: const Text('กรุณาใส่ตำแหน่ง'),
                  //       content: const Text('กรุณาปักหมุดบนแผนที่'),
                  //       actions: <Widget>[
                  //         TextButton(
                  //           onPressed: () => Navigator.pop(context, 'Cancel'),
                  //           child: const Text('ตกลง'),
                  //         ),
                  //       ],
                  //     ),
                  //   );
                  // } else if (name.isEmpty) {
                  //   showDialog<String>(
                  //     context: context,
                  //     builder: (BuildContext context) => AlertDialog(
                  //       title: const Text('กรุณาใส่ชื่อ'),
                  //       actions: <Widget>[
                  //         TextButton(
                  //           onPressed: () => Navigator.pop(context, 'Cancel'),
                  //           child: const Text('ตกลง'),
                  //         ),
                  //       ],
                  //     ),
                  //   );
                  // } else if (info.isEmpty) {
                  //   showDialog<String>(
                  //     context: context,
                  //     builder: (BuildContext context) => AlertDialog(
                  //       title: const Text('กรุณาใส่รายละเอียด'),
                  //       actions: <Widget>[
                  //         TextButton(
                  //           onPressed: () => Navigator.pop(context, 'Cancel'),
                  //           child: const Text('ตกลง'),
                  //         ),
                  //       ],
                  //     ),
                  //   );
                  // }
                },
                child: Text('Save'))
          ],
        ));
  }
}
