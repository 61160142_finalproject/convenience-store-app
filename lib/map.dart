import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:map_test/stores.dart';

import 'storeDetail.dart';

class MapWidget extends StatefulWidget {
  MapWidget({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MapWidgetStage createState() => _MapWidgetStage();
}

class _MapWidgetStage extends State<MapWidget> {
  GoogleMapController _controller;

  final Future<FirebaseApp> firebase = Firebase.initializeApp();
  CollectionReference _locationCollection =
      FirebaseFirestore.instance.collection("locations");

  final CameraPosition _initialPosition = CameraPosition(
      target: LatLng(13.279453610183248, 100.92627804980768), zoom: 15);

  final List<Marker> markers = [];

  updateMarker() async {
    markers.clear();
    var x = await _locationCollection.get();
    if (x != null) {
      x.docs.forEach((doc) {
        updateMarkers(
            doc.data()["location"]["Lat"],
            doc.data()["location"]["Lng"],
            doc.data()["name"],
            doc.data()["info"]);
      });
    }
  }

  addMarker(lat, lng) async {
    _locationCollection.add({
      'name': "โรงพยาบาลม.บูรพา",
      "location": {
        "Lat": lat,
        "Lng": lng,
      },
      'info': 'ตรงข้ามโรงพยาบาลม.บูรพา'
    });
    updateMarker();
  }

  updateMarkers(lat, lng, name, info) {
    int id = Random().nextInt(100);
    setState(() {
      markers.add(Marker(
          position: LatLng(lat, lng),
          markerId: MarkerId(id.toString()),
          infoWindow: InfoWindow(
              title: name,
              snippet: info,
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => StoreDetail(
                              id: name,
                            )));
              })));
    });
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: firebase,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return Scaffold(
              appBar: AppBar(
                title: Text("Error"),
              ),
              body: Center(
                child: Text("${snapshot.error}"),
              ),
            );
          }
          if (snapshot.connectionState == ConnectionState.done) {
            return Scaffold(
                appBar: AppBar(
                  title: Text(
                    "แผนที่",
                    style: GoogleFonts.prompt(),
                  ),
                  centerTitle: true,
                ),
                body: GoogleMap(
                  initialCameraPosition: _initialPosition,
                  mapType: MapType.normal,
                  onMapCreated: (controller) {
                    setState(() {
                      _controller = controller;
                      updateMarker();
                    });
                  },
                  markers: markers.toSet(),
                  onTap: (cordinate) {
                    _controller
                        .animateCamera(CameraUpdate.newLatLng(cordinate));
                    // addMarker(cordinate.latitude, cordinate.longitude);
                    // print(cordinate.latitude);
                  },
                ),
                floatingActionButton: FloatingActionButton(
                  backgroundColor: Colors.pink,
                  onPressed: () {
                    setState(() {
                      updateMarker();
                    });
                  },
                  child: Icon(Icons.refresh),
                ),
                bottomNavigationBar: BottomAppBar(
                  shape: CircularNotchedRectangle(),
                  notchMargin: 8.0,
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      IconButton(
                        icon: Icon(
                          Icons.arrow_back_outlined,
                        ),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                      ),
                      SizedBox(width: 120.0),
                      IconButton(
                        icon: Icon(
                          Icons.format_list_bulleted,
                        ),
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => StoreInformation()));
                        },
                      )
                    ],
                  ),
                ),
                floatingActionButtonLocation:
                    FloatingActionButtonLocation.centerDocked);
          }
          return Scaffold(
            body: Center(
              child: CircularProgressIndicator(),
            ),
          );
        });
  }
}
