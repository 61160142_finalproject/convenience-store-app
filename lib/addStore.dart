import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import 'map.dart';

class AddingStore extends StatefulWidget {
  AddingStore({Key key,}) : super(key: key);

  @override
  _AddingStoreState createState() => _AddingStoreState();
}

class _AddingStoreState extends State<AddingStore> {
  String userId;
  String name = '';
  String info = '';
  LatLng location = LatLng(0, 0);
  CollectionReference users = FirebaseFirestore.instance.collection('users');
  TextEditingController _nameController = new TextEditingController();
  TextEditingController _infoController = new TextEditingController();
  final List<Marker> markers = [];

  CollectionReference _locationCollection =
      FirebaseFirestore.instance.collection("locations");
  CollectionReference machines =
      FirebaseFirestore.instance.collection("machines");

  @override
  void initState() {
    super.initState();

    users.doc(this.userId).get().then((snapshot) {
      if (snapshot.exists) {
        var data = snapshot.data() as Map<String, dynamic>;
        setState(() {
          name = data['name'];
          info = data['info'];
          // age = data['age'].toString();
          _nameController.text = name;
          _infoController.text = info;
          // _locationController.text = age;
        });
      }
    });
  }

  final _formKey = GlobalKey<FormState>();

  updateMarkers(lat, lng) {
    // int id = Random().nextInt(100);
    markers.clear();
    setState(() {
      markers.add(Marker(
        position: LatLng(lat, lng),
        markerId: MarkerId('test'),
      ));
      // location = LatLng(lat, lng);
    });
  }

  addMarker() async {
    _locationCollection.add({
      'name': name,
      "location": {
        "Lat": location.latitude,
        "Lng": location.longitude,
      },
      'info': info
    });
    for (var i = 1; i <= 5; i++) {
      machines
          .add({
            'name': 'เครื่องที่ ' + i.toString(),
            'location_name': name,
            'lateUse': Timestamp.fromDate(
                    DateTime.now().subtract(const Duration(minutes: 60)))
                .toDate(),
            'status': true,
            'available': true
          })
          .then((value) => print('Add'))
          .catchError((error) => print('Failed to add user: $error'));
    }
  }

  GoogleMapController _controller;
  final CameraPosition _initialPosition = CameraPosition(
      target: LatLng(13.279453610183248, 100.92627804980768), zoom: 15);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            "เพิ่มร้าน",
            style: GoogleFonts.prompt(),
          ),
          centerTitle: true,
        ),
        body: ListView(
          padding: EdgeInsets.all(20),
          children: [
            Form(
                autovalidateMode: AutovalidateMode.onUserInteraction,
                key: _formKey,
                child: Column(
                  children: [
                    TextFormField(
                      controller: _nameController,
                      decoration: InputDecoration(labelText: 'ชื่อ'),
                      onChanged: (value) {
                        setState(() {
                          name = value;
                        });
                      },
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'กรุณาใส่ชื่อ';
                        }
                        return null;
                      },
                    ),
                    TextFormField(
                      controller: _infoController,
                      decoration: InputDecoration(labelText: 'รายละเอียด'),
                      onChanged: (value) {
                        setState(() {
                          info = value;
                        });
                      },
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'กรุณาใส่รายละเอียด';
                        }
                        return null;
                      },
                    ),
                  ],
                )),
            Divider(),
            Container(
              height: 300.0,
              child: GoogleMap(
                initialCameraPosition: _initialPosition,
                mapType: MapType.normal,
                onMapCreated: (controller) {
                  setState(() {
                    _controller = controller;
                    // updateMarker();
                  });
                },
                markers: markers.toSet(),
                onTap: (cordinate) {
                  _controller.animateCamera(CameraUpdate.newLatLng(cordinate));
                  location = LatLng(cordinate.latitude, cordinate.longitude);
                  updateMarkers(cordinate.latitude, cordinate.longitude);
                  // print(cordinate.latitude);
                },
              ),
            ),
            ElevatedButton(
                onPressed: () {
                  if (location.latitude == 0 || location.longitude == 0) {
                    showDialog<String>(
                      context: context,
                      builder: (BuildContext context) => AlertDialog(
                        title: const Text('กรุณาใส่ตำแหน่ง'),
                        content: const Text('กรุณาปักหมุดบนแผนที่'),
                        actions: <Widget>[
                          TextButton(
                            onPressed: () => Navigator.pop(context, 'Cancel'),
                            child: const Text('ตกลง'),
                          ),
                        ],
                      ),
                    );
                  } else if (name.isEmpty) {
                    showDialog<String>(
                      context: context,
                      builder: (BuildContext context) => AlertDialog(
                        title: const Text('กรุณาใส่ชื่อ'),
                        actions: <Widget>[
                          TextButton(
                            onPressed: () => Navigator.pop(context, 'Cancel'),
                            child: const Text('ตกลง'),
                          ),
                        ],
                      ),
                    );
                  } else if (info.isEmpty) {
                    showDialog<String>(
                      context: context,
                      builder: (BuildContext context) => AlertDialog(
                        title: const Text('กรุณาใส่รายละเอียด'),
                        actions: <Widget>[
                          TextButton(
                            onPressed: () => Navigator.pop(context, 'Cancel'),
                            child: const Text('ตกลง'),
                          ),
                        ],
                      ),
                    );
                  } else if (location.latitude != 0 &&
                      location.longitude != 0 &&
                      name.isNotEmpty &&
                      info.isNotEmpty) {
                    showDialog<String>(
                      context: context,
                      builder: (BuildContext context) => AlertDialog(
                        title: const Text('คุณต้องการเพิ่มร้านใช่หรือไม่'),
                        // content: const Text('กรุณาปักหมุดบนแผนที่'),
                        actions: <Widget>[
                          TextButton(
                            onPressed: () {
                              addMarker();
                              Navigator.pop(context, 'OK');
                              Navigator.pop(context, 'OK');
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => MapWidget()));
                            },
                            child: const Text('ยืนยัน'),
                          ),
                          TextButton(
                            onPressed: () {
                              Navigator.pop(context, 'Cancel');
                            },
                            child: const Text('ยกเลิก'),
                          ),
                        ],
                      ),
                    );
                    // Navigator.pop(context);
                  }
                },
                child: Text('Save'))
          ],
        ));
  }
}
