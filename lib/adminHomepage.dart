import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:map_test/addMachine.dart';
import 'package:map_test/addStore.dart';
import 'package:map_test/listEditDelStore.dart';
import 'package:map_test/map.dart';

import 'listEditDelMachine.dart';
import 'stores.dart';

class AdminHomepageWidget extends StatefulWidget {
  AdminHomepageWidget({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _AdminHomepageWidgetStage createState() => _AdminHomepageWidgetStage();
}

class _AdminHomepageWidgetStage extends State<AdminHomepageWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Homepage For Admin',
          style: GoogleFonts.prompt(),
        ),
        centerTitle: true,
      ),bottomNavigationBar: BottomAppBar(
        shape: CircularNotchedRectangle(),
        notchMargin: 8.0,
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            SizedBox(width: 200.0),
            IconButton(
              icon: Icon(
                Icons.format_list_bulleted,
              ),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => StoreInformation()));
              },
            ),
          ],
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.startDocked,
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.green[900],
        child: Icon(Icons.map_outlined),
        onPressed: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => MapWidget()));
        },
      ),
      body: ListView(
        children: [
          Card(
            child: new InkWell(
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => AddingStore()));
              },
              child: Container(
                height: 110,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(20)),
                child: Row(
                  children: [
                    Container(
                      width: 100.0,
                      height: 100.0,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15.0),
                          image: DecorationImage(
                              fit: BoxFit.cover,
                              image: NetworkImage(
                                  'https://firebasestorage.googleapis.com/v0/b/convenience-store-app.appspot.com/o/location2.png?alt=media&token=b4d92ee9-d846-4725-a36c-482a71d3840a'))),
                    ),
                    Expanded(
                      child: Container(
                        padding: EdgeInsets.only(top: 35, left: 10, right: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  'เพิ่มร้าน',
                                  style: GoogleFonts.prompt(
                                      fontSize: 20, height: 1.5),
                                ),
                                Icon(
                                  Icons.arrow_forward_ios_outlined,
                                  size: 15,
                                )
                              ],
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
          Card(
            child: new InkWell(
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => EditDelStore()));
              },
              child: Container(
                height: 110,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(20)),
                child: Row(
                  children: [
                    Container(
                      width: 100.0,
                      height: 100.0,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15.0),
                          image: DecorationImage(
                              fit: BoxFit.cover,
                              image: NetworkImage(
                                  'https://firebasestorage.googleapis.com/v0/b/convenience-store-app.appspot.com/o/location2.png?alt=media&token=b4d92ee9-d846-4725-a36c-482a71d3840a'))),
                    ),
                    Expanded(
                      child: Container(
                        padding: EdgeInsets.only(top: 35, left: 10, right: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  'แก้ไข/ลบร้าน',
                                  style: GoogleFonts.prompt(
                                      fontSize: 20, height: 1.5),
                                ),
                                Icon(
                                  Icons.arrow_forward_ios_outlined,
                                  size: 15,
                                )
                              ],
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
          Card(
            child: new InkWell(
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => AddingMachine()));
              },
              child: Container(
                height: 110,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(20)),
                child: Row(
                  children: [
                    Container(
                      width: 100.0,
                      height: 100.0,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15.0),
                          image: DecorationImage(
                              fit: BoxFit.cover,
                              image: NetworkImage(
                                  'https://firebasestorage.googleapis.com/v0/b/convenience-store-app.appspot.com/o/machine.png?alt=media&token=8a6d2c4e-47e9-4279-9ecd-8a19035ec80e'))),
                    ),
                    Expanded(
                      child: Container(
                        padding: EdgeInsets.only(top: 35, left: 10, right: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  'เพิ่มเครื่องซักผ้า',
                                  style: GoogleFonts.prompt(
                                      fontSize: 20, height: 1.5),
                                ),
                                Icon(
                                  Icons.arrow_forward_ios_outlined,
                                  size: 15,
                                )
                              ],
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
          Card(
            child: new InkWell(
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => EditDelMachine()));
              },
              child: Container(
                height: 110,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(20)),
                child: Row(
                  children: [
                    Container(
                      width: 100.0,
                      height: 100.0,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15.0),
                          image: DecorationImage(
                              fit: BoxFit.cover,
                              image: NetworkImage(
                                  'https://firebasestorage.googleapis.com/v0/b/convenience-store-app.appspot.com/o/machine.png?alt=media&token=8a6d2c4e-47e9-4279-9ecd-8a19035ec80e'))),
                    ),
                    Expanded(
                      child: Container(
                        padding: EdgeInsets.only(top: 35, left: 10, right: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  'แก้ไข/ลบเครื่องซักผ้า',
                                  style: GoogleFonts.prompt(
                                      fontSize: 20, height: 1.5),
                                ),
                                Icon(
                                  Icons.arrow_forward_ios_outlined,
                                  size: 15,
                                )
                              ],
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
